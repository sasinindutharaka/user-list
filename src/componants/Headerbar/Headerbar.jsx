import React from 'react';
import { Link } from 'react-router-dom';

function Headerbar() {
    return (
        <nav className="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
            
                <span className="navbar-brand mb-0 p-1 h1">USER LIST APPLICATION</span>
                <ul  className="navbar-nav">
                    <nav-link className="nav-item">
                        <Link className="nav-link" to="/">Home</Link>
                    </nav-link>
                    <li className="nav-item">
                        <Link className="nav-link" to="/addusers">Add Users</Link>
                    </li>
                    

                </ul>
                

            
        </nav>
    );
}
export default Headerbar;