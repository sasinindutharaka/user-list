import React,{useState} from 'react';
import { Button, Form } from 'react-bootstrap';
import axios from 'axios';
import {v4 as uuid4} from 'uuid'; 

function Addusers(){
const userId = uuid4();
const url = `https://user-list-f5f58-default-rtdb.firebaseio.com/userList/${userId}.json`;

const [data, setData] = useState({
name : " ",
city : " ",
email : " ",
imurl : " ",
dob : " "
})



function submit(e){
    e.preventDefault();
    axios.put(url,{Id: userId, name: data.name, city: data.city, email: data.email, imurl: data.imurl, dob: data.dob})
    .then(response => {
        window.alert("New User Added...");
        window.location.reload(); 
      })

}

function handle(e){
  const  newdata={...data}
    newdata[e.target.id]= e.target.value
    setData(newdata)
    console.log(newdata)
}
    return(
<div className="container">
      <div className="w-75 mx-auto shadow p-5 ">
        <h2 className="text-center mb-4">Add New User</h2>


<Form onSubmit={(e) => submit(e)} >
  <Form.Group className="mb-3 mt-3" controlId="formBasicEmail">
    
  <Form.Label>Enter name :</Form.Label>
    <Form.Control type="text"  className="mb-2" onChange={(e)=>handle(e)} id="name" value={data.name} />
    
    <Form.Label>Enter City :</Form.Label>
    <Form.Control type="text"  className="mb-2" onChange={(e)=>handle(e)} id="city" value={data.city} />

    <Form.Label>Enter Email :</Form.Label>
    <Form.Control type="email"  className="mb-2" onChange={(e)=>handle(e)} id="email" value={data.email} />

    <Form.Label>Enter Image URL Link :</Form.Label>
    <Form.Control type="text"  className="mb-2" onChange={(e)=>handle(e)} id="imurl" value={data.imurl} />

    <Form.Label>Enter Birthday :</Form.Label>
    <Form.Control type="date" onChange={(e)=>handle(e)} id="dob" value={data.dob} />
    
  </Form.Group>

  
  <Button className="mb-3 " variant="primary" type="submit">Add User</Button> 
  
  
  
</Form>
</div>
</div>
    );
}

export default Addusers;