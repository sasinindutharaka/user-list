import React from 'react';
import moment from 'moment';
import { Button,} from 'react-bootstrap';
import { Link } from 'react-router-dom';

function NameListItem(props) {
    return (

        <li className="list-group-item shadow ">
            <div className="row align-items-center">
                <div className="col-2">
                    <img src={props.avatar} alt={props.name} className="border border-3 rounded-circle" />
                </div>
                <div className="col-7">
                    <h4> {props.name}</h4>
                    <p>City: {props.city} | Email: {props.email}</p>
                    <small>Birthday: {moment(props.birthday).format('DD-MM-YYYY')}</small>
                </div>
                <div className="col-1">
                <Link to={`/editusers/${props.userInfo.Id}`}>
                <Button variant="secondary" >Edit</Button>
                </Link>
                </div>
                <div className="col-2">
                <Button className="" variant="danger" onClick = {() => props.onDelete(props.userInfo.Id )}>Delete</Button>
                
                </div>
            </div>

        </li>
 

    );
}
export default NameListItem;