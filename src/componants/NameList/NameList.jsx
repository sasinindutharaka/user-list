import React, { useState, useEffect } from 'react';
import NameListItem from './NameListItem';
import axios from 'axios';


function NameList() {
    
    const [nameList, setNameList] = useState([]);

    useEffect(() =>{

        const apiUrl = 'https://user-list-f5f58-default-rtdb.firebaseio.com/userList.json';
        axios.get(apiUrl).then(response =>{
            if (response.data){
                setNameList(Object.values(response.data));
               
            }
    
        })
    },[]);

   const handleDelete = (userId) => {
    const url = `https://user-list-f5f58-default-rtdb.firebaseio.com/userList/${userId}.json`;
    axios.delete(url).then((response) => {
       window.alert("Deleted..."); 
       window.location.reload(); 
    });

   };

    const nameListComponant = () => {
        return nameList.map((aUser) => {
            return (
                <NameListItem
                    key={aUser.id}
                    userId = {aUser.id}
                    name={aUser.name}
                    city={aUser.city}
                    email={aUser.email}
                    birthday={aUser.dob}
                    avatar={aUser.imurl}

                    userInfo = {aUser}
                    onDelete ={handleDelete}
                />
            )
        }

        );
    };

     

    return (

        <React.Fragment>
            <div className="container mt-4">
                {/* <button className="btn btn-primary mb-2" onClick={addUserHandler}>Add Name</button> */}
                <ul className="list-group">{nameListComponant()}</ul>
            </div>
        </React.Fragment>

    );
}
export default NameList;