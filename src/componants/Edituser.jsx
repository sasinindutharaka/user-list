import React, { useState, useEffect } from "react";
import axios from "axios";
import { Button, Form } from 'react-bootstrap';
import { useHistory, useParams } from "react-router-dom";



const EditUser = () => {
  let history = useHistory();
  const { id } = useParams();
  const [user, setUser] = useState({
    
    name: "",
    username: "",
    email: "",
    phone: "",
    website: ""
  });

  const { name, city, email, imurl, dob } = user;

  const onInputChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadUser();
  }, []);

  const onSubmit = async e => {
    e.preventDefault();
    await axios.put(`https://user-list-f5f58-default-rtdb.firebaseio.com/userList/${id}.json`, user);
    history.push("/");
  };

  const loadUser = async () => {
    const result = await axios.get(`https://user-list-f5f58-default-rtdb.firebaseio.com/userList/${id}.json`);
    setUser(result.data);
  };
  return (
    <div className="container">
      <div className="w-75 mx-auto shadow p-5 ">
        <h2 className="text-center mb-4">Edit A User</h2>
        

<Form onSubmit={(e) => onSubmit(e)} >
  <Form.Group className="mb-3 mt-3" controlId="formBasicEmail">
    
  <Form.Label>Enter name :</Form.Label>
    <Form.Control type="text"  className="mb-2" onChange ={(e)=> onInputChange(e)} name="name" value={name} />
    
    <Form.Label>Enter City :</Form.Label>
    <Form.Control type="text"  className="mb-2" onChange = {(e)=> onInputChange(e)} name="city" value={city} />

    <Form.Label>Enter Email :</Form.Label>
    <Form.Control type="email"  className="mb-2" onChange={(e)=>onInputChange(e)} name="email" value={email} />

    <Form.Label>Enter Image URL Link :</Form.Label>
    <Form.Control type="text"  className="mb-2" onChange={(e)=>onInputChange(e)} name="imurl" value={imurl} />

    <Form.Label>Enter Birthday :</Form.Label>
    <Form.Control type="date" onChange={(e)=>onInputChange(e)} name="dob" value={dob} />
    
  </Form.Group>

  
  <Button className="mb-3 btn btn-warning btn-block" variant="primary" type="submit">Update</Button> 
  
  
  
</Form>

      </div>
    </div>
  );
};

export default EditUser;