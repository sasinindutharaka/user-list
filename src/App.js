import logo from './logo.svg';
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import './App.css';
import NameList from './componants/NameList/NameList';
import Headerbar from './componants/Headerbar/Headerbar';
import Addusers from './componants/Addusers';
import Edituser from './componants/Edituser';

function App() {
  return (

    <div>
      
      <BrowserRouter>
        <Headerbar />
        <Switch>
          <Route path="/addusers">
            <Addusers />
          </Route>
         
          <Route path="/editusers/:id">
          <Edituser />
          </Route>

          <Route path="/">
            <NameList />
          </Route>

        </Switch>



      </BrowserRouter>


    </div>

  );
}

export default App;
